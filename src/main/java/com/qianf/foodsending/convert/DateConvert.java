package com.qianf.foodsending.convert;

import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 实现convert 接口
 * 时间转换器
 */
public class DateConvert implements Converter<String, Date> {

    private String dateFormat = "yyyy-MM-dd hh:mm:ss";


    /**
     * 将 "yyyy-MM-dd hh:mm:ss  ----》 Date
     *    2021-05-13%2011:08:26 ---Date
     * @param s
     * @return
     */
    @Override
    public Date convert(String s) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        try {
            return simpleDateFormat.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  null;
    }
}
