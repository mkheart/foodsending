package com.qianf.foodsending.convert;

import org.springframework.format.Formatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * 实现 Formatter 完成 时间转换
 *
 * yyyy-MM-dd hh:mm:ss --》date
 */
public class DateFormatter2 implements Formatter<Date> {

    private String dateFormat = "yyyy-MM-dd";

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);

    @Override // 解析 字符转 yyyy-MM-dd ---》date
    public Date parse(String s, Locale locale) throws ParseException {
        return simpleDateFormat.parse(s);
    }

    @Override // 将 date --> yyyy-MM-dd
    public String print(Date date, Locale locale) {
        return simpleDateFormat.format(date);
    }
}
