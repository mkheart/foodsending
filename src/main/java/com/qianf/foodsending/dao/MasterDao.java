package com.qianf.foodsending.dao;


import com.qianf.foodsending.entity.Order;
import com.qianf.foodsending.entity.OrderDetail;
import com.qianf.foodsending.entity.OrderMaster;
import org.springframework.stereotype.Repository;


@Repository
public interface MasterDao {


    int addOrderMaster(Order order);

    int addOrderDetail(OrderDetail orderDetail);
    OrderMaster masterById(String orderId);
    int updateMaster(OrderMaster orderMaster);
}
