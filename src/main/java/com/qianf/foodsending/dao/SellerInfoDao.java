package com.qianf.foodsending.dao;

import com.qianf.foodsending.entity.SellerInfo;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface SellerInfoDao {

    /**
     * 查找所有商户
     * @return
     */
    List<SellerInfo> findAllSellerInfo();


    /**
     * 折线图
     * @return
     */
    List<Map> findAllOrderDetailNum();

    /**
     * 根据商家名字查询商家信息
     * @param username
     * @return
     */
    SellerInfo findSellerByName(String username);

    /**
     * 查询所有订单数量
     */
    int findSellerNum();

    /**
     * 查询商户入驻天数
     * @return
     */
    int findSellerDay();

}
