package com.qianf.foodsending.dao;

import com.qianf.foodsending.entity.OrderMaster;

import java.util.List;

public interface OrderDao {


    /**
     * 查询所有订单信息
     * @return
     */
    List<OrderMaster> findAllOrderMaster (String openId);


    /**
     * 查询订单信息
     * @return
     */
    OrderMaster findOrderDetail (String orderId);
}
