package com.qianf.foodsending.dao;

import com.qianf.foodsending.entity.Category;

import com.qianf.foodsending.entity.ProductCategory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryDao {
    /**
     * 查询所有商品类目
     * @return
     */
    List<Category> findAllCategory();

    /**
     * 添加商品类目
     * @return
     */
    int addCategory(Category category);

    /**
     * 修改类目
     * @param category
     * @return
     */
    int updateCategory(Category category);

    /**
     * 删除类目
     * @param categoryId
     * @return
     */
    int deleteCategory(int categoryId);



    /**
     * 查询所有商品类目
     * @return
     */
    List<Category> findAllProductInfoCategory();


}
