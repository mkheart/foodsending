package com.qianf.foodsending.dao;

import com.qianf.foodsending.entity.Foods;
import com.qianf.foodsending.entity.ProductAndCategory;
import com.qianf.foodsending.entity.ProductInfo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductInfoDao {

    /**
     * 查询商品列表
     * @return
     */
    List<ProductInfo> findAllProductInfo ();


    /**
     * 更新商品下架
     * @param productId
     * @return
     */
    int updateProductInfo (String productId);


    /**
     * 根据id查询商品信息
     * @param productId
     * @return
     */
    ProductInfo getProductById (String productId);


    /**
     * 更新商品信息
     * @param productInfo
     * @return
     */
    int updateProduct (ProductInfo productInfo);


    /**
     * 添加商品信息
     * @param productInfo
     * @return
     */
    int insertProductInfo (ProductInfo productInfo);


    /**
     * 查询商品类别
     * @return
     */
    List<ProductAndCategory> listProduct ();


//    /**
//     * 查询所有商品信息
//     * @return
//     */
//    List<Foods> listFoods ();




}
