package com.qianf.foodsending.dao;

import com.qianf.foodsending.entity.UserInfo;
import org.springframework.stereotype.Repository;


@Repository
public interface UserLoginDao {


    /**
     * 查询用户名是否在数据库中
     * 并且核实用户密码进行登录
     * @param name
     * @return
     */
    UserInfo loginUser(String name);


    /**
     * 注册用户
     * @param userInfo
     * @return
     */
    int registerUser (UserInfo userInfo);

}
