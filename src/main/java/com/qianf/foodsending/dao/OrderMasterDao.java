package com.qianf.foodsending.dao;

import com.qianf.foodsending.entity.OrderMaster;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderMasterDao {

    /**
     * 查询所有的订单信息
     * @return
     */
    List<OrderMaster> findAllOrderMaster();

    OrderMaster findOrderMasterById(String id);
}
