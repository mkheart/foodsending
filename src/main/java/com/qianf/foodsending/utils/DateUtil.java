package com.qianf.foodsending.utils;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public static long getDate(Date date) {
        //获取指定时间的时间戳，除以1000说明得到的是秒级别的时间戳（10位）
        long time = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse(String.valueOf(date), new ParsePosition(0)).getTime() / 1000;

        //获取时间戳
        return time;

    }

}
