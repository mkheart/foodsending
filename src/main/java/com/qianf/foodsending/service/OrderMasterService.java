package com.qianf.foodsending.service;

import com.qianf.foodsending.entity.OrderMaster;

import java.util.List;

public interface OrderMasterService {

    /**
     * 查询所有的订单信息
     * @return
     */
    List<OrderMaster> findAllOrderMaster(Integer page, Integer limit);

    /**
     * 根据订单id查询单条订单信息
     * @param id
     * @return
     */
    OrderMaster findOrderMasterById(String id);
}
