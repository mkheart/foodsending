package com.qianf.foodsending.service;

import com.qianf.foodsending.entity.OrderDetailNum;
import com.qianf.foodsending.entity.SellerInfo;

import java.util.List;

public interface SellerInfoService {

    /**
     * 查找所有商户
     * @return
     */
    List<SellerInfo> findAllSellerInfo();

    /**
     * 折线图
     * @return
     */
    OrderDetailNum findAllOrderDetailNum();


    /**
     * 根据商户名称查询商户信息
     * @return
     */
    SellerInfo findSellerByName(String username);

    /**
     * 查询所有订单数量
     */
    int findSellerNum();

    /**
     * 查询商家入驻天数
     * @return
     */
    int findSellerDay();

}
