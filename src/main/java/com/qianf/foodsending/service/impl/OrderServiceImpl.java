package com.qianf.foodsending.service.impl;

import com.qianf.foodsending.dao.OrderDao;
import com.qianf.foodsending.entity.OrderDetail;
import com.qianf.foodsending.entity.OrderMaster;
import com.qianf.foodsending.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;

    @Override
    public List<OrderMaster> findAllOrderMaster(String openId) {
        return orderDao.findAllOrderMaster(openId);
    }

    @Override
    public OrderMaster findOrderDetail(String orderId) {
        return orderDao.findOrderDetail(orderId);
    }
}
