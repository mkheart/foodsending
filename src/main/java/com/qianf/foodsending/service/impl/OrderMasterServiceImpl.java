package com.qianf.foodsending.service.impl;

import com.qianf.foodsending.dao.OrderMasterDao;
import com.qianf.foodsending.entity.OrderMaster;
import com.qianf.foodsending.service.OrderMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class OrderMasterServiceImpl implements OrderMasterService {

    @Autowired
    private OrderMasterDao orderMasterDao;

    @Override
    public List<OrderMaster> findAllOrderMaster(Integer page, Integer limit) {
        return orderMasterDao.findAllOrderMaster();
    }

    @Override
    public OrderMaster findOrderMasterById(String id) {
        return orderMasterDao.findOrderMasterById(id);
    }

}
