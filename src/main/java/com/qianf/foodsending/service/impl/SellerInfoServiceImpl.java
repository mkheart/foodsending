package com.qianf.foodsending.service.impl;

import com.qianf.foodsending.dao.SellerInfoDao;
import com.qianf.foodsending.entity.OrderDetailNum;
import com.qianf.foodsending.entity.SellerInfo;
import com.qianf.foodsending.service.SellerInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class SellerInfoServiceImpl implements SellerInfoService {

    @Autowired
    private SellerInfoDao sellerInfoDao;

    public List<SellerInfo> findAllSellerInfo() {
        return sellerInfoDao.findAllSellerInfo();
    }

    @Override
    public OrderDetailNum findAllOrderDetailNum() {

        List<Map> orderDetailNumMap = sellerInfoDao.findAllOrderDetailNum();

        List<String> orderDetailList = new ArrayList();
        List<Float> orderDetailNumList = new ArrayList();

        for (Map map : orderDetailNumMap) {
            String orderDetailData = map.get("orderTime").toString() ;
            float productPrice = Float.parseFloat(map.get("ordertotal").toString());

            orderDetailList.add(orderDetailData);
            orderDetailNumList.add(productPrice);
        }

        OrderDetailNum orderDetailNum = new OrderDetailNum(orderDetailList,orderDetailNumList);


        return orderDetailNum;
    }

    @Override
    public SellerInfo findSellerByName(String username) {
        return sellerInfoDao.findSellerByName(username);
    }


    @Override
    public int findSellerNum() {
        return sellerInfoDao.findSellerNum();
    }

    @Override
    public int findSellerDay() {
        return sellerInfoDao.findSellerDay();
    }
}
