package com.qianf.foodsending.service.impl;

import com.qianf.foodsending.dao.ProductInfoDao;
import com.qianf.foodsending.entity.Foods;
import com.qianf.foodsending.entity.ProductAndCategory;
import com.qianf.foodsending.entity.ProductInfo;
import com.qianf.foodsending.service.ProductInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductInfoServiceImpl implements ProductInfoService {

    @Autowired
    private ProductInfoDao productInfoDao;

    @Override
    public List<ProductInfo> findAllProductInfo(Integer page, Integer limit) {
        return productInfoDao.findAllProductInfo();
    }

    @Override
    public int updateProductInfo(String productId) {
        return productInfoDao.updateProductInfo(productId);
    }

    @Override
    public ProductInfo getProductById(String productId) {
        return productInfoDao.getProductById(productId);
    }

    @Override
    public int updateProduct(ProductInfo productInfo) {
        return productInfoDao.updateProduct(productInfo);
    }

    @Override
    public int insertProductInfo(ProductInfo productInfo) {
        return productInfoDao.insertProductInfo(productInfo);
    }

    @Override
    public List<ProductAndCategory> listProduct() {
        return productInfoDao.listProduct();
    }

//    @Override
//    public List<Foods> listFoods() {
//        return productInfoDao.listFoods();
//    }
}
