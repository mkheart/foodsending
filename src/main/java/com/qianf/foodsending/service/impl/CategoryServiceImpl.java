package com.qianf.foodsending.service.impl;

import com.qianf.foodsending.dao.CategoryDao;
import com.qianf.foodsending.entity.Category;
import com.qianf.foodsending.entity.ProductCategory;
import com.qianf.foodsending.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryDao categoryDao;


    @Override
    public List<Category> findAllCategory() {
        return categoryDao.findAllCategory();
    }

    @Override
    public int addCategory(Category category) {
        return categoryDao.addCategory(category);
    }

    @Override
    public int updateCategory(Category category) {
        return categoryDao.updateCategory(category);
    }

    @Override
    public int deleteCategory(int categoryId) {
        return categoryDao.deleteCategory(categoryId);
    }
    @Override
    public List<Category> findAllProductInfoCategory() {
        return categoryDao.findAllProductInfoCategory();

    }


}

