package com.qianf.foodsending.service.impl;

import com.qianf.foodsending.dao.UserLoginDao;
import com.qianf.foodsending.entity.UserInfo;
import com.qianf.foodsending.service.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserLoginServiceImpl implements UserLoginService {

    @Autowired
    private UserLoginDao userLoginDao;

    @Override
    public UserInfo loginUser(String name) {

        return userLoginDao.loginUser(name);

    }

    @Override
    public int registerUser(UserInfo userInfo) {
        return userLoginDao.registerUser(userInfo);
    }
}
