package com.qianf.foodsending.service.impl;



import com.qianf.foodsending.dao.MasterDao;
import com.qianf.foodsending.entity.Order;
import com.qianf.foodsending.entity.OrderDetail;
import com.qianf.foodsending.entity.OrderMaster;
import com.qianf.foodsending.service.MasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MasterServiceImpl implements MasterService {

    @Autowired
    private MasterDao masterDao;


    @Override
    public int addOrderMaster(Order order) {
        return masterDao.addOrderMaster(order);
    }

    @Override
    public int addOrderDetail(OrderDetail orderDetail) {
        return masterDao.addOrderDetail(orderDetail);
    }

    @Override
    public OrderMaster masterById(String orderId) {
        return masterDao.masterById(orderId);
    }

    @Override
    public int updateMaster(OrderMaster orderMaster) {
        return masterDao.updateMaster(orderMaster);
    }
}
