package com.qianf.foodsending.service;




import com.qianf.foodsending.entity.Order;
import com.qianf.foodsending.entity.OrderDetail;
import com.qianf.foodsending.entity.OrderMaster;

import java.util.List;

public interface MasterService {


    int addOrderMaster(Order order);
    int addOrderDetail(OrderDetail orderDetail);
    OrderMaster masterById(String orderId);
    int updateMaster(OrderMaster orderMaster);
}
