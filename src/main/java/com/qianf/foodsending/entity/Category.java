package com.qianf.foodsending.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class Category implements Serializable {

    /**类目id*/
    private int categoryId;

    /** 类目名字 */
    private String categoryName;

    /** 类目编号 */
    private int categoryType;

    /** 创建时间 */
    //1、将json 中的时间 转换为 java date
    //2、返回json 数据可以将 date 转为yyyy-MM-dd HH:mm:ss
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    //接收对象时 将属性 yyyy-MM-dd HH:mm:ss 转换为date类型
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

    /** 修改时间 */
    //1、将json 中的时间 转换为 java date
    //2、返回json 数据可以将 date 转为yyyy-MM-dd HH:mm:ss
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    //接收对象时 将属性 yyyy-MM-dd HH:mm:ss 转换为date类型
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date updateTime;

    public Category() {
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(int categoryType) {
        this.categoryType = categoryType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "Category{" +
                "categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                ", categoryType=" + categoryType +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
