package com.qianf.foodsending.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ProductCategory implements Serializable {
    /**类目id*/
    private int id;

    /** 类目名字 */
    private String name;

    /** 类目编号 */
    private int type;

    private List<Info> foods;
    /** 创建时间 */
    //1、将json 中的时间 转换为 java date
    //2、返回json 数据可以将 date 转为yyyy-MM-dd HH:mm:ss
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    //接收对象时 将属性 yyyy-MM-dd HH:mm:ss 转换为date类型
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

    /** 修改时间 */
    //1、将json 中的时间 转换为 java date
    //2、返回json 数据可以将 date 转为yyyy-MM-dd HH:mm:ss
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    //接收对象时 将属性 yyyy-MM-dd HH:mm:ss 转换为date类型
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date updateTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<Info> getFoods() {
        return foods;
    }

    public void setFoods(List<Info> foods) {
        this.foods = foods;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "ProductCategory{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", foods=" + foods +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
