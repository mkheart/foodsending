package com.qianf.foodsending.entity;

import java.io.Serializable;
import java.util.Date;

public class Foods implements Serializable {

    private String id;
    private String name;
    private String price;
    private String icon;
    private String productStatus;
    private String categoryType;
    private String productStock;
    private String productIcon;
    private Date createTime;
    private Date updateTime;


    public Foods () {

    }

    public Foods(String id, String name, String price, String icon, String productStatus, String categoryType, String productStock, String productIcon, Date createTime, Date updateTime) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.icon = icon;
        this.productStatus = productStatus;
        this.categoryType = categoryType;
        this.productStock = productStock;
        this.productIcon = productIcon;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getProductStock() {
        return productStock;
    }

    public void setProductStock(String productStock) {
        this.productStock = productStock;
    }

    public String getProductIcon() {
        return productIcon;
    }

    public void setProductIcon(String productIcon) {
        this.productIcon = productIcon;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "Foots{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", price='" + price + '\'' +
                ", icon='" + icon + '\'' +
                ", productStatus='" + productStatus + '\'' +
                ", categoryType='" + categoryType + '\'' +
                ", productStock='" + productStock + '\'' +
                ", productIcon='" + productIcon + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
