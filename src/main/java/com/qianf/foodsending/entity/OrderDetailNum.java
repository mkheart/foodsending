package com.qianf.foodsending.entity;

import java.io.Serializable;
import java.util.List;

public class OrderDetailNum implements Serializable {

    private List<String> orderTimeList;

    private List<Float> ordertotalList;

    //入驻时长
    private int totalDay;

    //总订单
    private int orderTotalNum;

    public int getTotalDay() {
        return totalDay;
    }

    public void setTotalDay(int totalDay) {
        this.totalDay = totalDay;
    }

    public float getOrderTotalNum() {
        return orderTotalNum;
    }

    public void setOrderTotalNum(int orderTotalNum) {
        this.orderTotalNum = orderTotalNum;
    }

    public OrderDetailNum() {
    }

    public OrderDetailNum(List<String> orderTimeList, List<Float> ordertotalList) {
        this.orderTimeList = orderTimeList;
        this.ordertotalList = ordertotalList;
    }


    public List<String> getOrderTimeList() {
        return orderTimeList;
    }

    public void setOrderTimeList(List<String> orderTimeList) {
        this.orderTimeList = orderTimeList;
    }

    public List<Float> getOrdertotalList() {
        return ordertotalList;
    }

    public void setOrdertotalList(List<Float> ordertotalList) {
        this.ordertotalList = ordertotalList;
    }

    @Override
    public String toString() {
        return "OrderDetailNum{" +
                "orderTimeList=" + orderTimeList +
                ", ordertotalList=" + ordertotalList +
                ", totalDay=" + totalDay +
                ", orderTotalNum=" + orderTotalNum +
                '}';
    }
}
