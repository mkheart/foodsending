package com.qianf.foodsending.entity;

import java.io.Serializable;
import java.util.List;

public class ProductAndCategory implements Serializable {


    private String type;

    private String name;

    private List<Foods> foods;

    public ProductAndCategory () {

    }

    public ProductAndCategory(String type, String name, Foods foots) {
        this.type = type;
        this.name = name;
        this.foods = foods;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Foods> getFoods() {
        return foods;
    }

    public void setFoods(List<Foods> foods) {
        this.foods = foods;
    }

    @Override
    public String toString() {
        return "ProductAndCategory{" +
                "id='" + type + '\'' +
                ", name='" + name + '\'' +
                ", foods=" + foods +
                '}';
    }
}
