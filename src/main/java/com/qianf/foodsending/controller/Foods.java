package com.qianf.foodsending.controller;


import com.qianf.foodsending.entity.ProductAndCategory;
import com.qianf.foodsending.entity.ResponseData;
import com.qianf.foodsending.service.ProductInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/buyer/product")
@RestController
public class Foods {

    @Autowired
    private ProductInfoService productInfoService;


    @RequestMapping("/list")
    public ResponseData list () {

        List<ProductAndCategory> productAndCategories = productInfoService.listProduct();

        ResponseData<Object> responseData = new ResponseData();

        responseData.setData(productAndCategories);

        return responseData;


    }

}
