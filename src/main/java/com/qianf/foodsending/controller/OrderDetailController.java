package com.qianf.foodsending.controller;

import com.qianf.foodsending.entity.OrderDetailNum;
import com.qianf.foodsending.entity.ResponseData;
import com.qianf.foodsending.service.SellerInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 折线图
 */
@RestController
public class OrderDetailController {

    @Autowired
    private SellerInfoService sellerInfoService;

    /**
     * 折线图
     * @return
     */
    @RequestMapping("/groupByDept.do")
    public ResponseData<OrderDetailNum> groupByDept(){

        ResponseData<OrderDetailNum> responseData = new ResponseData();

        OrderDetailNum orderDetailNumList = sellerInfoService.findAllOrderDetailNum();
        //总订单信息
        int num = sellerInfoService.findSellerNum();
        //商家入驻时长
        int day = sellerInfoService.findSellerDay();

        orderDetailNumList.setOrderTotalNum(num);
        orderDetailNumList.setTotalDay(day);

        if (orderDetailNumList == null){
            responseData.setCode(1);
            return responseData;
        }
        responseData.setCode(0);
        responseData.setData(orderDetailNumList);

        return responseData;
    }


}
