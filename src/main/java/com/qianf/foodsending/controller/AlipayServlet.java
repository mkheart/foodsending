package com.qianf.foodsending.controller;


import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.github.pagehelper.Constant;
import com.qianf.foodsending.config.AlipayConfig;
import com.qianf.foodsending.config.ApplicationContextHold;
import com.qianf.foodsending.entity.OrderMaster;
import com.qianf.foodsending.service.MasterService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 支付宝 支付相关
 */
@WebServlet("/alipayServlet.do")
public class AlipayServlet extends BaseServlet {


    /**
     * 调用支付宝 后台进行订单支付
     * @param req
     * @param resp
     * @return
     * @throws ServletException
     * @throws IOException
     */
    public String pay(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //实例化客户端,填入所需参数
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.GATEWAY, AlipayConfig.APP_ID, AlipayConfig.APP_PRIVATE_KEY, AlipayConfig.FORMAT, AlipayConfig.CHARSET, AlipayConfig.ALIPAY_PUBLIC_KEY, AlipayConfig.SIGN_TYPE);
        AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();

        //在公共参数中设置回跳和通知地址
        request.setReturnUrl(AlipayConfig.RETURN_URL);
        request.setNotifyUrl(AlipayConfig.NOTIFY_URL);


        //根据订单编号,查询订单相关信息
//        Order order = payService.selectById(orderId);
        //商户订单号，商户网站订单系统中唯一订单号，必填
        String order_no = req.getParameter("orderno");
        //付款金额，必填
        String total_amount =  req.getParameter("amount");
        //订单名称，必填
        String subject = req.getParameter("productname");


        //商品描述，可空  生成二维码进行支付
        String body = "";
        request.setBizContent("{\"out_trade_no\":\""+ order_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");


        String form = "";
        try {
            form = alipayClient.pageExecute(request).getBody(); // 调用SDK生成表单
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }

        return form;// 直接将完整的表单html输出到页面

    }

    /**
     * 付款成功的回调
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws AlipayApiException
     */
    public String returnUrl(HttpServletRequest request, HttpServletResponse response)
            throws IOException, AlipayApiException {
        System.out.println("=================================同步回调=====================================");

        //获取支付宝GET过来反馈信息
        Map<String, String> params = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            //输出信息
            System.out.println(name+"---->>"+ Arrays.toString(values));
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用
            valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }

        // 商户订单号
        String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"), "UTF-8");

        // 支付宝交易号
        String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"), "UTF-8");

        // 付款金额
        String total_amount = new String(request.getParameter("total_amount").getBytes("ISO-8859-1"), "UTF-8");

        System.out.println("商户订单号=" + out_trade_no);
        System.out.println("支付宝交易号=" + trade_no);
        System.out.println("付款金额=" + total_amount);

        //支付成功 修改订单状态 2  改变订单已支付  更具订单编号修改
        MasterService masterService = ApplicationContextHold.getApplicationContext().getBean(MasterService.class);
        OrderMaster orderMaster = masterService.masterById(out_trade_no);
        orderMaster.setPayStatus(1);
        orderMaster.setOrderId(out_trade_no);
        masterService.updateMaster(orderMaster);
//
//        Order orderMaster = orderMasterService.getById(out_trade_no);
//
//        orderMaster.setPayStatus(Constant.PayStatusSUCCESS);
//
//        orderMasterService.updateById(orderMaster);


        // 也可以返回订单列表
        return "redirect:" + "http://localhost/#/order/";//跳转付款成功页面

    }

}
