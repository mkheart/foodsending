package com.qianf.foodsending.controller;


import com.qianf.foodsending.entity.OrderDetail;
import com.qianf.foodsending.entity.OrderMaster;
import com.qianf.foodsending.entity.ResponseData;
import com.qianf.foodsending.entity.UserInfo;
import com.qianf.foodsending.service.OrderService;
import com.qianf.foodsending.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RequestMapping("/buyer/order")
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;


    @RequestMapping("/list")
    public ResponseData listOrder (HttpSession httpSession) {

        UserInfo user = (UserInfo) httpSession.getAttribute("user");

        String openId = user.getOpenid();

        List<OrderMaster> orderMasterList = orderService.findAllOrderMaster(openId);


        ResponseData<Object> responseData = new ResponseData();

        responseData.setCode(0);
        responseData.setMsg("成功");
        responseData.setData(orderMasterList);

        return responseData;
    }


    @RequestMapping("/detail")
    public ResponseData detailOrder (String orderId) {
        ResponseData<Object> responseData = new ResponseData();

        OrderMaster orderDetail = orderService.findOrderDetail(orderId);

        responseData.setCode(0);
        responseData.setMsg("成功");
        responseData.setData(orderDetail);

        return responseData;

    }

}
