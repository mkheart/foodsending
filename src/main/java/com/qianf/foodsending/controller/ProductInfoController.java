package com.qianf.foodsending.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qianf.foodsending.utils.AliOssUtil;
import com.qianf.foodsending.utils.KeyUtil;
import com.qianf.foodsending.entity.ProductInfo;
import com.qianf.foodsending.entity.ResponseData;
import com.qianf.foodsending.service.ProductInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@RequestMapping("/meishipai/product-info")
@RestController
public class ProductInfoController {

    @Autowired
    private ProductInfoService productInfoService;

    @RequestMapping("/list")
    public ResponseData selectProductInfo (int page,int limit) {

        //开启分页信息
        PageHelper.startPage(page,limit);

        //查询信息
        List<ProductInfo> productCategory = productInfoService.findAllProductInfo(page, limit);

        //得到分页信息
        PageInfo pageInfo = new PageInfo(productCategory);

        ResponseData<Object> responseData = new ResponseData();

        responseData.setCode(0);
        responseData.setCount((int) pageInfo.getTotal());
        responseData.setData(productCategory);

        return responseData;
    }


    @RequestMapping("/updateGrade.do")
    public ResponseData  updateGrade(String productId) {

        int i = productInfoService.updateProductInfo(productId);

        ResponseData<Object> responseData = new ResponseData();

        if (i > 0) {

            responseData.setCode(0);
            responseData.setInfo("下架成功");

            return responseData;

        } else {
            responseData.setCode(1);
            responseData.setInfo("下架失败");

            return responseData;
        }

    }


    @RequestMapping("/getProductById")
    public ResponseData  getProductById (String productId) {

        ProductInfo productById = productInfoService.getProductById(productId);

        ResponseData responseData = new ResponseData();

        responseData.setData(productById);

        return responseData;

    }


    @RequestMapping("/updateProduct")
    public ResponseData updateProduct (ProductInfo productInfo) {


        int i = productInfoService.updateProduct(productInfo);

        ResponseData<Object> responseData = new ResponseData();

        if (i > 0) {
            responseData.setMsg("更新成功");

            return responseData;
        } else {
            responseData.setMsg("更新失败");

            return responseData;
        }

    }


    @RequestMapping("/fileUpload")
    public ResponseData uploadFile(List<MultipartFile> file) throws IOException {

        String url= "";
        if (file!=null && file.size()>0){

            for (MultipartFile multipartFile:file){

                String filename =   multipartFile.getOriginalFilename();

                String filePath = "D:\\image"+File.separator+ UUID.randomUUID() +filename;
                File localfile = new File(filePath);

                multipartFile.transferTo(localfile);


                // 上传阿里云
                FileInputStream fis=new FileInputStream(filePath);
                ByteArrayOutputStream baos=new ByteArrayOutputStream();
                byte[] data=new byte[1024];
                int len;
                while ((len=fis.read(data))!=-1){
                    baos.write(data,0,len);
                }
                url= AliOssUtil.uploadByte("imagewgz","1.PNG",baos.toByteArray());
                System.out.println("url = " + url);
            }
        }

        ResponseData responseData = new ResponseData<String>();
//        responseData.setData(url);

        return responseData;

    }

    @RequestMapping("/addProduct")
    public ResponseData fileUpload (ProductInfo productInfo,List<MultipartFile> file) throws IOException {

        Date date = new Date();

        String uniqueKey = KeyUtil.genUniqueKey();

        productInfo.setCreateTime(date);

        productInfo.setProductId(uniqueKey);

        int i = productInfoService.insertProductInfo(productInfo);

        ResponseData<Object> responseData = new ResponseData();

        if (i > 0 ) {

            responseData.setMsg("添加成功");

            return responseData;

        } else {

            responseData.setMsg("添加失败");

            return responseData;

        }

    }






}
