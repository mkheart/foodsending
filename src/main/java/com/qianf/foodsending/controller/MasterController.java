package com.qianf.foodsending.controller;


import com.alibaba.fastjson.JSON;
import com.qianf.foodsending.entity.*;
import com.qianf.foodsending.service.MasterService;
import com.qianf.foodsending.service.ProductInfoService;
import com.qianf.foodsending.utils.KeyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.math.BigDecimal;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class MasterController {

    @Autowired
    private MasterService masterService;
    @Autowired
    private ProductInfoService productInfoService;


    @RequestMapping("/buyer/order/create")
    public ResponseData addMaster(Order order) {

        int orderAmount2 = 0;
        BigDecimal orderAmount4 = new BigDecimal(orderAmount2);
        String items = order.getItems();
        List<Item> items1 = JSON.parseArray(items, Item.class);
        String uniqueKey = KeyUtil.genUniqueKey();
        order.setOrderId(uniqueKey);

        for (Item item : items1) {
            OrderDetail orderDetail = new OrderDetail();

            String productId = item.getProductId();
            int productQuantity = item.getProductQuantity();
            ProductInfo productById = productInfoService.getProductById(productId);
            float productPrice = productById.getProductPrice();
            String productName = productById.getProductName();
            String productIcon = productById.getProductIcon();

            // 把订单添加到订单详情
            String uniqueKey1 = KeyUtil.genUniqueKey();
            orderDetail.setDetailId(uniqueKey1);
            orderDetail.setOrderId(order.getOrderId());
            orderDetail.setProductId(productId);
            orderDetail.setProductName(productName);
            orderDetail.setProductPrice(productPrice);
            orderDetail.setProductQuantity(productQuantity);
            orderDetail.setProductIcon(productIcon);
            masterService.addOrderDetail(orderDetail);


            BigDecimal bigDecimal = new BigDecimal(productPrice);
            BigDecimal bigDecima2 = new BigDecimal(productQuantity);
            BigDecimal orderAmount1 = bigDecimal.multiply(bigDecima2);
            orderAmount4 = orderAmount4.add(orderAmount1);


        }
        ResponseData<Object> responseData = new ResponseData();
        order.setOrderAmount(orderAmount4);
        int i = masterService.addOrderMaster(order);
        Map<String, String> map = new HashMap();
        map.put("orderId", order.getOrderId());
        map.put("orderAmount", order.getOrderAmount().toString());

        if (i > 0) {
            responseData.setCode(0);
            responseData.setMsg("成功");
            responseData.setData(map);
            return responseData;
        } else {
            responseData.setCode(1);
            responseData.setMsg("失败");

            return responseData;
        }
    }
}

