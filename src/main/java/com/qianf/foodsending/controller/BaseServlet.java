package com.qianf.foodsending.controller;





import com.qianf.foodsending.config.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * BaseServlet 提供给所有的Servlet程序，完成对应的方法处理过程
 */
public abstract class BaseServlet extends HttpServlet {
    @Override
    public void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");

        // exam: method == listStudent
        String method = req.getParameter("method");

        if (method==null){// 如果传 method 则直接跳转首页
            System.out.println("错误：没有传递method方法名，跳转首页");
            method = "redirectIndex";
        }

        Method targetMethod = null;
            // public void listStudent(HttpServletRequest req, HttpServletResponse resp)
        try {
            targetMethod = this.getClass().getMethod(method, HttpServletRequest.class, HttpServletResponse.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        // 执行目标
        Object result = null;
        try {
            result = targetMethod.invoke(this, req, resp);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        if (result != null) {
                //转发 重定向  返回字符
                String str = (String) result;
                // 返回值 以 forward: 开头    forward:/index.jsp
                if (str.startsWith(Constants.FORWARD)) {
                    //转发     截取   : 后面的字符串 forward:/index.jsp----> /index.jsp
                    String path = str.substring(str.indexOf(Constants.FLAG) + 1);

                    // 转发 index.jsp
                    req.getRequestDispatcher(path).forward(req,resp);
                }else if (str.startsWith(Constants.REDIRECT)){
                    // 如果 以 redirect:/index.jsp 开头
                    //重定向  redirect:/index.jsp  -----> /index.jsp
                    String path = str.substring(str.indexOf(Constants.FLAG) + 1);

                    //重定向到  /index.jsp
                    resp.sendRedirect(path);
                }else{  // 直接返回响应数据
                    resp.getWriter().println(str);
                }
            }else {  // 什么都不做

            }

        }



    /**
     * 重定向到 首页
     * @param req
     * @param resp
     * @return
     */
    public String  redirectIndex(HttpServletRequest req, HttpServletResponse resp){

        return Constants.REDIRECT+"/index.jsp";
    }
}
