package com.qianf.foodsending.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qianf.foodsending.entity.Category;
import com.qianf.foodsending.entity.ProductCategory;
import com.qianf.foodsending.entity.ResponseData;
import com.qianf.foodsending.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * 商品类目列表查询
     *
     * @param page
     * @param limit
     * @return
     */
    @RequestMapping("/meishipai/product-category/list")
    public ResponseData<Category> findAllCategory(int page, int limit) {

        PageHelper.startPage(page, limit);
        List<Category> categoryList = categoryService.findAllCategory();
        // 得到分页相关信息
        PageInfo pageInfo = new PageInfo(categoryList);

        ResponseData responseData = new ResponseData();

        responseData.setCode(0);
        responseData.setMsg("success");
        responseData.setCount((int) pageInfo.getTotal());
        responseData.setData(categoryList);

        return responseData;
    }

    /**
     * 添加商品类目
     * @param category
     * @return
     */
    @RequestMapping("/meishipai/product-category/addCategory")
    public ResponseData addCategory(Category category) {

        ResponseData<Object> responseData = new ResponseData();


        int i = categoryService.addCategory(category);
        if (i > 0) {
            responseData.setCode(0);
            responseData.setMsg("成功");
            return responseData;
        } else {
            responseData.setCode(1);
            responseData.setMsg("失败");
            return responseData;
        }


    }
    /**
     * 商品类目列表修改
     *
     * @param category
     * @return
     */
    @RequestMapping("/meishipai/product-category/update")
    public ResponseData update(Category category) {
        Date date = new Date();
        category.setUpdateTime(date);
        category.setCreateTime(date);
        ResponseData responseData = new ResponseData();
        int i = categoryService.updateCategory(category);
        if (i > 0) {
            responseData.setCode(0);
            responseData.setMsg("修改成功");
            return responseData;
        } else {
            responseData.setCode(1);
            responseData.setMsg("修改失败");
            return responseData;
        }
    }

    /**
     * 根据id 删除商品类目
     *
     * @param categoryId
     * @return
     */
    @RequestMapping("/meishipai/product-category/delete")
    public ResponseData delete(int categoryId) {
        ResponseData responseData = new ResponseData();
        int i = categoryService.deleteCategory(categoryId);
        if (i > 0) {
            responseData.setCode(0);
            responseData.setMsg("删除成功");
            return responseData;
        } else {
            responseData.setCode(1);
            responseData.setMsg("删除失败");
            return responseData;
        }
    }

        @RequestMapping("/meishipai/product-category/listCategory")
        public ResponseData categoryNameList() {

            List<Category> categoryList = categoryService.findAllProductInfoCategory();

            ResponseData<Object> responseData = new ResponseData();


            responseData.setCode(0);
            responseData.setData(categoryList);

            return responseData;


        }



    }
