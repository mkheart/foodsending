package com.qianf.foodsending.controller;

import com.github.pagehelper.PageHelper;
import com.qianf.foodsending.entity.ResponseData;

import com.qianf.foodsending.entity.SellerInfo;
import com.qianf.foodsending.service.SellerInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RequestMapping("/meishipai/seller-info")
@RestController
public class SellerInfoController {


    @Autowired
    private SellerInfoService sellerInfoService;

    @RequestMapping("/findAllSellerInfo")
    public List<SellerInfo> findAllSellerInfo(int pageIndex, int pageSize) {

        PageHelper.startPage(pageIndex, pageSize);
        return sellerInfoService.findAllSellerInfo();
    }

    /**
     * 判断当前用户是否 登录成功
     *
     * @return
     */
    @RequestMapping("/LoginToJudge.do")
    @ResponseBody
    public ResponseData loginToJudge(HttpSession session) {

        ResponseData<Object> responseData = new ResponseData();

        SellerInfo seller = (SellerInfo) session.getAttribute("sellerInfo");
        if (seller != null) {//当前会话恢复登录过
            responseData.setCode(0);
            responseData.setMsg("登录成功");
        } else {
            responseData.setCode(1);
            responseData.setMsg("尚未登录");
        }
        return responseData;
    }

    /**
     * 商户登录
     *
     * @param username    商户名称
     * @param pass
     * @param httpSession
     * @return
     */
    @RequestMapping("/login.do")
    public ResponseData loginToJudge(String username, String pass, HttpSession httpSession) {
        ResponseData<Object> responseData = new ResponseData();
        SellerInfo seller = sellerInfoService.findSellerByName(username);

        if (seller == null) {
            responseData.setCode(1);
            responseData.setMsg("用户不存在");
            return responseData;
        }

        if (!seller.getPassword().equals(pass)) {
            responseData.setCode(1);
            responseData.setMsg("用户名或者密码错误");
            return responseData;
        }

        httpSession.setAttribute("sellerInfo", seller);
        responseData.setCode(0);
        return responseData;
    }

    /**
     * 获取当前用户信息
     *
     * @param session
     * @return
     */
    @RequestMapping("/findByNo.do")
    @ResponseBody
    public ResponseData<SellerInfo> findSellerInfo(HttpSession session) {

        ResponseData<SellerInfo> userResponseData = new ResponseData();
        SellerInfo SellerInfo = (SellerInfo) session.getAttribute("SellerInfo");

        userResponseData.setCode(0);
        userResponseData.setData(SellerInfo);

        return userResponseData;
    }

    /**
     * 注销
     *
     * @param session
     * @return
     */
    @RequestMapping("/logout")
    public ResponseData logout(HttpSession session) {
        ResponseData<Object> responseData = new ResponseData();

        session.invalidate();//清除session信息 退出登录

        responseData.setCode(0);
        responseData.setMsg("退出登录成功");

        return responseData;

    }

}
