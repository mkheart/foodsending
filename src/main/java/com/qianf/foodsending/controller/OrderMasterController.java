package com.qianf.foodsending.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qianf.foodsending.entity.OrderMaster;
import com.qianf.foodsending.entity.ResponseData;
import com.qianf.foodsending.service.OrderMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/seller/order")
@RestController
public class OrderMasterController {

    @Autowired
    private OrderMasterService orderMasterService;

    @RequestMapping("/list")
    public ResponseData findAllOrderMaster(int page,int limit){
        // 开启分页
        PageHelper.startPage(page, limit);

        List<OrderMaster> departList = orderMasterService.findAllOrderMaster(page, limit);
        // 得到分页相关信息
        PageInfo pageInfo = new PageInfo(departList);

        ResponseData<Object> responseData = new ResponseData();

        responseData.setCode(0);
        responseData.setCount((int) pageInfo.getTotal());
        responseData.setData(departList);

        return responseData;
    }

    @RequestMapping("/findOrderMasterById")
    public OrderMaster findOrderMasterById(String id){
        OrderMaster orderMaster = orderMasterService.findOrderMasterById(id);

        return orderMaster;
    }
}
