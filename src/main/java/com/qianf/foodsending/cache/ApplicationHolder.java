package com.qianf.foodsending.cache;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 *
 * ApplicationContextAware  作用 就是 在生成 对应的实例是调用，将容器ApplicationContext 传进来
 */
@Component// 将当前类加入到容器中  ，如果当前类实现ApplicationContextAware 还会在创建bean调用改接口对应的setApplicationContext
public class ApplicationHolder implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        // 持有上下文对象
        this.applicationContext  = applicationContext;
    }


    /**
     * 获取核心容器
     * @return
     */
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}
