package com.qianf.foodsending.cache;

import org.apache.ibatis.cache.Cache;
import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 自定义 mybatis 二级缓存  底层使用 redis 存储
 *
 * 当前 RedisCache 实例 没有加入到容器ApplicationContext
 *
 */

public class RedisCache implements Cache {

    private  String id;

    // 读写锁  可以多个人同时读，只要有一个人写 其他人都不能读
    private  ReadWriteLock readWriteLock = new ReentrantReadWriteLock();



    public RedisCache() {
    }

    public RedisCache(String id) {
        System.out.println("命名空间-----id："+id);
        this.id = id;
    }

    // 获取的id 就是 当前mapper 的命名空间
    public String getId() {
        return id;
    }


    /**
     * 向缓存中存放数据   key（类似sql） value (查询结果)
     * @param o
     * @param o1
     */
    public void putObject(Object key, Object value) {
        System.out.println("存储  key = " + key);
            getRedisTemplate().opsForValue().set(key.toString(), value, 10, TimeUnit.MINUTES);
    }

    /**
     * 获取缓存是 通过 key
     * @param key
     * @return
     */
    public Object getObject(Object key) {
        System.out.println("获取key 对应的值  key = " + key);

        return getRedisTemplate().opsForValue().get(key.toString());
    }

    /**
     * 清除key 对应的缓存
     * @param key
     * @return
     */
    public Object removeObject(Object key) {
        System.out.println("删除  key = " + key);
        return getRedisTemplate().delete(key.toString());
    }

    /**
     * 清空当前命名空间二级缓存
     *          就是删除所有 对应namespace（id） 的 key
     */
    public void clear() {
        // 获取到所有包含 namespace（id） 的 key
        Set<String> keys = getRedisTemplate().keys("*" + id + "*");

        for (String key : keys) {
            System.out.println("遍历清空所有的namespace 下 的key = " + key);
            getRedisTemplate().delete(key);
        }

    }

    /**
     * 获取当前命名空间对应的条数
     * @return
     */
    public int getSize() {

        System.out.println("获取当前命名空间下 的条数");
        // 获取到所有包含 namespace（id） 的 key
        Set<String> keys = getRedisTemplate().keys("*" + id + "*");
        return keys.size();
    }

    public ReadWriteLock getReadWriteLock() {
        return readWriteLock;
    }


    /**
     * 获取容器中的 redisTemplate
     * @return
     */
    public  RedisTemplate getRedisTemplate(){

        // 获取到容器
        ApplicationContext applicationContext = ApplicationHolder.getApplicationContext();

        return (RedisTemplate) applicationContext.getBean("redisTemplate");
    }
}
